<?php

public function foo($str)
{
    $sql = "SELECT * from foo WHERE {$str}";

    $db = Ak::db();
    $db->execute($sql);

}

public function bar($str)
{
    $boop = "SELECT * from foo WHERE {$str}";

    $nothing = Ak::db();
    $nothing->execute($boop);
}

public function foobar($str)
{
    $sql = "SELECT * from nowhere WHERE {$str}";

    $this->findBySql($sql);
}

public function foobarAgain($str)
{
    $nothing = "SELECT * from nowhere WHERE {$str}";

    $this->findOne($nothing);
}



public function activities()
{
    $tmp = $this->ActivityLog->findBySql(
        "select activity from activity_logs group by activity order by activity"
    );

    $activities = $this->ActivityLog->collect($tmp, 'activity', 'activity');
    $this->renderJSON($activities);
}

public function sqlInjection($str)
{
    $appName = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $connStr = "host=localhost port=5432 dbname=postgres user=postgres options='--application_name=$appName'";

    $conn = pg_connect($connStr);
    $result = pg_query($conn, "select * nowhere where id = {$str}");
    var_dump(pg_fetch_all($result));
}

